<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    //
    
     protected $table = 'coach';
     protected $fillable = [
        'title', 'bio', 'phone','email_id','link_of_youtube','status'
    ];
}
