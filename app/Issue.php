<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Issue extends Model
{
	use SoftDeletes;
	protected $table = 'issue';

    
    protected $primaryKey = 'id';
	
    protected $guarded = [];

    public function category()
    {
        return $this->hasMany('App\Categories', 'category_id', 'id');
    }
	public function subcategory()
    {
        return $this->hasMany('App\Categories', 'child_category_id', 'id');
    }
	public function survey()
    {
        return $this->belongsTo('App\Survey', 'survey_id', 'id');
    }
	
}
