<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Classes;
use Illuminate\Http\Request;
use Session;
use App\Refefile;
use Illuminate\Support\Facades\File;
use DataTables;
class ClassesController extends Controller
{
    //
     public function __construct()
    {
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function classDatatable(Request $request) {
        $record = Classes::all();

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        return view('admin.classes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		
        return view('admin.classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => 'required|unique:classes',
                'description' => 'required',
                'link_of_youtube' => 'required|url', 
               'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
            ]);

        $data = $request->except('profile_image');
      
		
        $classes = Classes::create($data);
        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/classes/'.$classes->id,'classes',$classes->id,'classes_image',[]);
            }
        
        Session::flash('flash_message', __('Class added!'));

		
        return redirect('admin/classes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $class = Classes::where("id",$id)->first();

        if(!$class){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
                
        $refefile=Refefile::where('refe_field_id',$class->id)->where('refe_table_field_name','classes')->orderby('created_at','DESC')->first();

        return view('admin.classes.show', compact('class','refefile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
       $class = Classes::where("id",$id)->first();

       
        if(!$class){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
        $refefile=Refefile::where('refe_field_id',$class->id)->where('refe_table_field_name','classes')->orderby('created_at','DESC')->first();

	
		return view('admin.classes.edit', compact('class','refefile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
                'title' => 'required',
                'description' => 'required',
                'link_of_youtube' => 'required|url', 
                'status' => 'required',
                'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

        $data = $request->except('profile_image');

        $classes = Classes::where("id",$id)->first();

        if(!$classes){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if ($classes) {
            $classes->update($data);
            $classes->save();
        }

        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/classes/'.$classes->id,'classes',$classes->id,'classes_image',[]);
            }
		
        Session::flash('flash_message', __('Classes updated!'));

        return redirect('admin/classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
         $ob = Classes::where("id",$id)->first();

        if($ob){
           
            $ob->delete();
            
			$result['message'] = 'Record deleted Success';
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/users');
        }
    }

    
    
    
    
    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }

}
