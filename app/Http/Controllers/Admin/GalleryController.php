<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use Session;
use App\Refefile;
use Illuminate\Support\Facades\File;
use DataTables;
use App\GalleryCategory;


class GalleryController extends Controller
{
    //

	 public function __construct()
    {
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function galleryDatatable(Request $request) {
        $record = Gallery::all();

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        return view('admin.gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		$category=GalleryCategory::where('status','active')->pluck('title', 'id');
        return view('admin.gallery.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => 'required|unique:classes',
                'description' => 'required',
                'category_id' => 'required', 
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
            ]);

        $data = $request->except('profile_image');
      
		
        $gallery = Gallery::create($data);
        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/gallery/'.$gallery->id,'gallery',$gallery->id,'gallery_image',[]);
            }
        
        Session::flash('flash_message', __('Gallery added!'));

		
        return redirect('admin/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $gallery = Gallery::with('category')->where("id",$id)->first();

        if(!$gallery){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
                
        $refefile=Refefile::where('refe_field_id',$gallery->id)->where('refe_table_field_name','gallery')->orderby('created_at','DESC')->first();

        return view('admin.gallery.show', compact('gallery','refefile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
       $gallery = Gallery::where("id",$id)->first();
       $category=GalleryCategory::where('status','active')->pluck('title', 'id');
   
        if(!$gallery){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
        $refefile=Refefile::where('refe_field_id',$gallery->id)->where('refe_table_field_name','gallery')->orderby('created_at','DESC')->first();

		return view('admin.gallery.edit', compact('gallery','refefile','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
                'title' => 'required',
                'description' => 'required',
                'category_id' => 'required', 
                'status' => 'required',
                'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

        $data = $request->except('profile_image');

        $gallery = Gallery::where("id",$id)->first();

        if(!$gallery){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if ($gallery) {
            $gallery->update($data);
            $gallery->save();
        }

        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/gallery/'.$gallery->id,'gallery',$gallery->id,'gallery_image',[]);
            }
		
        Session::flash('flash_message', __('Gallery updated!'));

        return redirect('admin/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
         $ob = Gallery::where("id",$id)->first();

        if($ob){
           
            $ob->delete();
            
			$result['message'] = 'Record deleted Success';
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/users');
        }
    }

}
