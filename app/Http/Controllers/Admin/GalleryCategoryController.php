<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GalleryCategory;
use Session;
use App\Refefile;
use Illuminate\Support\Facades\File;
use DataTables;
use App\Gallery;

class GalleryCategoryController extends Controller
{
    //
      public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function galleryCategoryDatatable(Request $request) {
        $record = GalleryCategory::all();

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        return view('admin.gallerycategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		
        return view('admin.gallerycategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => 'required|unique:gallery_category',
                'status' => 'required'
            ]);

        $data = $request->all();
      
		
        $gallery_category = GalleryCategory::create($data);
        
        Session::flash('flash_message', __('Gallery Category added!'));

		
        return redirect('admin/gallery-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $gallery_category = GalleryCategory::where("id",$id)->first();

        if(!$gallery_category){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
                
        $refefile=Refefile::where('refe_field_id',$gallery_category->id)->where('refe_table_field_name','gallery_category')->orderby('created_at','DESC')->first();

        return view('admin.gallerycategory.show', compact('gallery_category','refefile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
       $gallery_category = GalleryCategory::where("id",$id)->first();

       
        if(!$gallery_category){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
        $refefile=Refefile::where('refe_field_id',$gallery_category->id)->where('refe_table_field_name','gallery_category')->orderby('created_at','DESC')->first();

	
		return view('admin.gallerycategory.edit', compact('gallery_category','refefile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
                'title' => 'required',
               
            ]);

        $data = $request->all();

        $gallery_category = GalleryCategory::where("id",$id)->first();

        if(!$gallery_category){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if ($gallery_category) {
            $gallery_category->update($data);
            $gallery_category->save();
        }

       if($request->status == 'Inactive')
       {
            $gallery= Gallery::where('category_id',$id)->update(['status'=>'Inactive']);
       }
		
        Session::flash('flash_message', __('Gallery Category updated!'));

        return redirect('admin/gallery-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
         $ob = GalleryCategory::where("id",$id)->first();

        if($ob){
           Gallery::where('category_id',$ob->id)->delete();
            $ob->delete();
            
			$result['message'] = 'Record deleted Success';
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/users');
        }
    }

    

}
