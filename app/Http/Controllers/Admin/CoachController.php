<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coach;
use Session;
use App\Refefile;
use Illuminate\Support\Facades\File;
use DataTables;

class CoachController extends Controller
{
    //
      public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function coachDatatable(Request $request) {
        $record = Coach::all();

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        return view('admin.coach.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		
        return view('admin.coach.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => 'required|unique:coach',
                'bio' => 'required',
                'phone' => 'required|min:11|numeric',
                'email_id' => 'required|unique:coach',
                'link_of_youtube' => 'required|url', 
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
            ]);

        $data = $request->except('profile_image');
      
		
        $coach = Coach::create($data);
        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/coach/'.$coach->id,'coach',$coach->id,'coach_image',[]);
            }
        
        Session::flash('flash_message', __('Coach added!'));

		
        return redirect('admin/coach');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $coach = Coach::where("id",$id)->first();

        if(!$coach){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
                
        $refefile=Refefile::where('refe_field_id',$coach->id)->where('refe_table_field_name','coach')->orderby('created_at','DESC')->first();

        return view('admin.coach.show', compact('coach','refefile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
       $coach = Coach::where("id",$id)->first();

       
        if(!$coach){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
        $refefile=Refefile::where('refe_field_id',$coach->id)->where('refe_table_field_name','coach')->orderby('created_at','DESC')->first();

	
		return view('admin.coach.edit', compact('coach','refefile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
              'title' => 'required',
                'bio' => 'required',
                'phone' => 'required|min:11|numeric',
                'email_id' => 'required',
                'link_of_youtube' => 'required|url', 
                'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
            ]);

        $data = $request->except('profile_image');

        $coach = Coach::where("id",$id)->first();

        if(!$coach){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if ($coach) {
            $coach->update($data);
            $coach->save();
        }

        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/coach/'.$coach->id,'blogs',$coach->id,'coach_image',[]);
            }
		
        Session::flash('flash_message', __('Coach updated!'));

        return redirect('admin/coach');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
         $ob = Coach::where("id",$id)->first();

        if($ob){
           
            $ob->delete();
            
			$result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/coach');
        }
    }

}
