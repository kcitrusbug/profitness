<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use Session;
use App\Refefile;
use Illuminate\Support\Facades\File;
use DataTables;

class BlogController extends Controller
{
    // //
     public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function blogDatatable(Request $request) {
        $record = Blog::all();

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        return view('admin.blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'title' => 'required|unique:blog',
                'description' => 'required',
                'link_of_youtube' => 'required|url', 
                'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
            ]);

        $data = $request->except('profile_image');
      
		
        $blogs = Blog::create($data);
        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/blogs/'.$blogs->id,'blogs',$blogs->id,'blogs_image',[]);
            }
        
        Session::flash('flash_message', __('Blog added!'));

		
        return redirect('admin/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $blog = Blog::where("id",$id)->first();

        if(!$blog){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
                
        $refefile=Refefile::where('refe_field_id',$blog->id)->where('refe_table_field_name','blogs')->orderby('created_at','DESC')->first();

        return view('admin.blogs.show', compact('blog','refefile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
       $blog = Blog::where("id",$id)->first();

       
        if(!$blog){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
        $refefile=Refefile::where('refe_field_id',$blog->id)->where('refe_table_field_name','blogs')->orderby('created_at','DESC')->first();

	
		return view('admin.blogs.edit', compact('blog','refefile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
                'title' => 'required',
                'description' => 'required',
                'link_of_youtube' => 'required|url', 
                'status' => 'required',
                'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

        $data = $request->except('profile_image');

        $blogs = Blog::where("id",$id)->first();

        if(!$blogs){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if ($blogs) {
            $blogs->update($data);
            $blogs->save();
        }

        if($request->hasFile('profile_image'))
            {
                $files = [$request->file('profile_image')];
                uploadModalReferenceFile($files,'uploads/blogs/'.$blogs->id,'blogs',$blogs->id,'blogs_image',[]);
            }
		
        Session::flash('flash_message', __('Blogs updated!'));

        return redirect('admin/blogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
         $ob = Blog::where("id",$id)->first();

        if($ob){
           
            $ob->delete();
            
			$result['message'] = 'Record deleted Success';
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/users');
        }
    }

    
    
    
    
    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }

}
