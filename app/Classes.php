<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    //
     protected $table = 'classes';
     protected $fillable = [
        'profile_image','title', 'description', 'link_of_youtube','status'
    ];

    public function refefile()
    {
        return $this->hasOne('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'classes');
    }
}
