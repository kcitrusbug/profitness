<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    
     protected $table = 'gallery';
     protected $fillable = [
        'image','title', 'description', 'category_id','status'
    ];

	public function category()
    {
        return $this->hasOne('App\GalleryCategory', 'id','category_id');
    }
}
