@push('css')
<style>
    .intl-tel-input{
        display: block;
    }
</style>
@endpush

<div class="row ">

    <lable class="col-md-1"></lable>
    <div class="col-md-6">

        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            <label for="title" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.title')
            </label>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('bio') ? 'has-error' : ''}}">
            <label for="bio" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.bio')
            </label>
                {!! Form::textarea('bio', null, ['class' => 'form-control']) !!}
                {!! $errors->first('bio', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('link_of_youtube') ? 'has-error' : ''}}">
            <label for="link_of_youtube" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.link_of_youtube')
            </label>
                {!! Form::text('link_of_youtube', null, ['class' => 'form-control']) !!}
                {!! $errors->first('link_of_youtube', '<p class="help-block">:message</p>') !!}

        </div>
         <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
            <label for="phone" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.phone')
            </label>
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}

        </div>
         <div class="form-group {{ $errors->has('email_id') ? 'has-error' : ''}}">
            <label for="email_id" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.email_id')
            </label>
                {!! Form::email('email_id', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email_id', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('profile_image') ? 'has-error' : ''}}">
            <label for="profile_image" class="">
                <span class="field_compulsory">*</span>@lang('coach.label.profile_image')
            </label>
                {!! Form::file('profile_image', ['accept'=>"image/*",'class' => 'form-control','multiple'=>false]) !!}
                {!! $errors->first('profile_image', '<p class="help-block">:message</p>') !!}

        </div>

        @if(isset($coach))
            @if($refefile->file_thumb_url != "" )
                        <img src="{!! $refefile->file_thumb_url !!}" height="80" />
                        @else
                        <img src="{!! $refefile->file_url !!}" height="80" />
            @endif
        @endif

		<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', trans('common.label.status'), ['class' => '']) !!}
            {!! Form::select('status',trans('common.active_status'), null, ['class' => 'form-control']) !!}
        </div>
        

        
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>







@push('js')
<script>

    
    
    
    $('.selectTag2').select2({
            tokenSeparators: [",", " "]
     });

    

</script>


@endpush

