@push('css')
<style>
    .intl-tel-input{
        display: block;
    }
</style>
@endpush

<div class="row ">

    <lable class="col-md-1"></lable>
    <div class="col-md-6">

        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            <label for="title" class="">
                <span class="field_compulsory">*</span>@lang('class.label.title')
            </label>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

        </div>

		<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', trans('common.label.status'), ['class' => '']) !!}
            {!! Form::select('status',trans('common.active_status'), null, ['class' => 'form-control']) !!}
        </div>
        

        
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>







@push('js')
<script>

    
    
    
    $('.selectTag2').select2({
            tokenSeparators: [",", " "]
     });

    

</script>


@endpush

