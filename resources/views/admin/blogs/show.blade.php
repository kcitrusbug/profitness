@extends('layouts.apex')

@section('title',trans('blog.label.show_blog'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('blog.label.show_blog')</div>
               
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/blogs') }}" title="Back">
                            <button class="btn btn-raised btn-success round btn-min-width mr-1 mb-1"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('common.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                      </div>  
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                   <div class="table-responsive">
                                        <table class="table table-striped">
                                            <tbody>

                                            <tr>
                                                <td>@lang('common.label.id')</td>
                                                <td>{{ $blog->id }}</td>
                                            </tr>
                                            <tr>

                                                <td>@lang('blog.label.title')</td>
                                                <td> {{ $blog->title }} </td>
                                            </tr>
											
											<tr>

                                                <td>@lang('blog.label.link_of_youtube')</td>
                                                <td> {{ $blog->link_of_youtube }} </td>
                                            </tr>
											<tr>

                                                <td>@lang('blog.label.description')</td>
                                                <td> {{ $blog->description }} </td>
                                            </tr>
                                            <tr>

                                                <td>@lang('blog.label.profile_image')</td>
                                                <td>  @if($refefile->file_thumb_url != "" )
                        <img src="{!! $refefile->file_thumb_url !!}" height="80" />
                        @else
                        <img src="{!! $refefile->file_url !!}" height="80" />
            @endif </td>
                                            </tr>

											<tr>

                                                <td>@lang('common.label.status')</td>
                                                <td> {{ $blog->status }} </td>
                                            </tr>
                          
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


     