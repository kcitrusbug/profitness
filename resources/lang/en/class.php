<?php

return [


    'label'=>[
        'classes'=>'Classes',
        'class'=>'Class',
        'id'=>'ID',
        'profile_image'=>'Profile Image',
        'title'=>'Title',
        'description'=>'Description',
        'link_of_youtube'=>'Link of youtube',
        'create_new_class'=>'Create New Class',
        'create_class'=>'Create Class',
        'edit_class'=>'Edit Class',
        'show_class'=>'Show Class',
        'select_class'=>'Select Class',
        'status'=>'Status',
        
    ]
];