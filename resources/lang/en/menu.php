<?php

return [
    'label' => [
        'Users' => 'Users',
        'Users' => 'Users',
        'Roles' => 'Roles',
        'Permissions' => 'Permissions',
        'Give_Role_Permissions' => 'Give Role Permissions',
        'Location' => 'Location',
        'Settings' => 'Settings',
        'Constants' => 'Constants',
        'DropdownsTypes' => 'List Types',
        'DropdownValues' => 'List Values',
        'Languages' => 'Languages',
        'Categories' => 'Categories',
        'Survey' => 'Survey',
        'Class' => 'Class',
        'Blogs' => 'Blogs',
        'GalleryCategory' => 'GalleryCategory',
        'Gallery' => 'Gallery',
        'Coach' => 'Coach',
    ],

];