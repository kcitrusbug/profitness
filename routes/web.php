<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('sign-in', 'Auth\LoginController@loginA');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/','ProfileController@index');
	
	/*Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/edit', 'ProfileController@edit');
    Route::patch('/profile/edit', 'ProfileController@update');
	
	Route::get('/profile/change-password', 'ProfileController@changePassword');
    Route::patch('/profile/change-password', 'ProfileController@updatePassword');*/
	
}); 


Route::group(['prefix' => 'admin','middleware' => ['auth', 'roles'],'roles' => 'AU'], function () {
	Route::get('/', 'Admin\AdminController@index');
	
	
	Route::get('roles/datatable', 'Admin\RolesController@datatable');
    Route::resource('/roles', 'Admin\RolesController');
	
	Route::get('/users/search', 'Admin\UsersController@search');
    Route::get('/users/datatable', 'Admin\UsersController@userDatatable');
    Route::resource('/users', 'Admin\UsersController');

    Route::get('/classes/search', 'Admin\ClassesController@search');
    Route::get('/classes/datatable', 'Admin\ClassesController@classDatatable');
    Route::resource('/classes', 'Admin\ClassesController');

    Route::get('/blogs/search', 'Admin\BlogController@search');
    Route::get('/blogs/datatable', 'Admin\BlogController@blogDatatable');
    Route::resource('/blogs', 'Admin\BlogController');
	
	Route::get('/gallery-category/search', 'Admin\GalleryCategoryController@search');
    Route::get('/gallery-category/datatable', 'Admin\GalleryCategoryController@galleryCategoryDatatable');
    Route::resource('/gallery-category', 'Admin\GalleryCategoryController');

	Route::get('/gallery/search', 'Admin\GalleryController@search');
    Route::get('/gallery/datatable', 'Admin\GalleryController@galleryDatatable');
    Route::resource('/gallery', 'Admin\GalleryController');

    Route::get('/coach/search', 'Admin\CoachController@search');
    Route::get('/coach/datatable', 'Admin\CoachController@coachDatatable');
    Route::resource('/coach', 'Admin\CoachController');

	Route::resource('permissions', 'Admin\PermissionsController');
	
	
	Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
    Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
    Route::patch('/profile/edit', 'Admin\ProfileController@update');
        //
    Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
    Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
	
	
});  




